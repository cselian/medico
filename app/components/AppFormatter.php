<?php

namespace app\components;
use app\models\Tag;
use app\models\Post;
use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

class AppFormatter
{
	public static function formatAll($model, $view)
	{
		$view->registerJsFile("@web/assets/qrcode.min.js",['depends' => [\yii\web\JqueryAsset::className()]]);
		$model->links = self::profile($model->links, $view);
	}
	
	private static function profile($raw, $view)
	{
		$lines = explode(PHP_EOL, $raw);
		$name = false;
		foreach ($lines as $line) {
			if (strpos($line, 'id:') !== false)
			{
				$name = trim(str_replace('id:', '', $line));
				break;
			}
		}
		if (!$name) return '<em>PROFILE REQUIRES "id" field';
		
		$op = array(sprintf('<a name="%s"></a>', $name));
		$lnk = '<a href="%s" target="_blank">%s</a>';
		$pic = '<div style="float: right"><img src="%s" width="180" /></div>';

		$url = Url::current([], true) . '#' . $name;
		$qrcode = sprintf('<div id="profile-%s" style="float: left; margin-right: 15px;"></div>', $name);
		$op[] = $qrcode . sprintf($lnk, $url, $url);

		$view->registerJs(sprintf('new QRCode("profile-%s", { text: "%s", width: 80, height: 80, });', $name, $url), View::POS_READY, 'profile');

		foreach ($lines as $line)
		{
			$line = trim($line);
			if ($line == '') continue;
			if ($line == '<!--more-->') break; //support only one for now
			if (strpos($line, ':') !== false) {
				$bits = explode(':', $line, 2);
				$bits[1] = trim($bits[1]);

				if (strcasecmp($bits[0], 'pic') == 0) {
					$op[] = sprintf($pic, $bits[1]);
					continue;
				} else if (strcasecmp($bits[0], 'link') == 0) {
					$url = explode('|', $bits[1]);
					$bits[1] = sprintf($lnk, $url[0], count($url) == 1 ? $url[0] : $url[1] );
				}
				$op[] = '<b>' . $bits[0] . ':</b> ' . $bits[1];
			} else {
				$op[] = $line;
			}
		}
		return PHP_EOL . '<div class="profile" style="page-break-before: always;">' . implode('<br/>' . PHP_EOL, $op) . '</div>';
		//$op .= '<pre>' . $raw . '</pre>';
	}
}
?>