<?php

namespace app\components;
use app\models\Tag;
use app\models\Post;
use Yii;
use yii\helpers\ArrayHelper;

class AppHelper
{
    public static $quillSettings = [ 'toolbarOptions' => 'FULL' ];

    public static $datePickerSettings = [ 'dateFormat' => 'yyyy-M-dd' ];

    public static function init() {
        static::setFormatter();
    }

    public static function setFormatter() {
        Yii::$app->getFormatter()->dateFormat = 'd MMM yyyy H:i:s a';
    }

    public static function date($date, $time = true) {
        if ($date == null) return "";
        $dt = strtotime($date);
        //if ($compact) return sprintf('<span class="date compact" title="%s">%s</span>', date('d M Y', $dt), date('D h:i', $dt));
        $time = $time ? ' h:i' : '';
        return date('D, d M Y' . $time, $dt);
    }

    public static function html($html)
    {
        return str_replace('href', 'target="_blank" href', $html);
    }

    public static function now()
    {
        return date('Y-m-d H:i:s'); //\yii\db\Expression('NOW()'); //https://stackoverflow.com/a/42083654
    }

    public static function hasNoLocations()
    {
      return \app\models\Location::find()->count() == 0;
    }

    public static function hasNoUsers()
    {
      return \app\models\User::find()->count() == 0;
    }

    public static function checkLoginOnSave()
    {
        if (Yii::$app->user->isGuest) throw new \yii\web\HttpException(403, 'must be signed in to save');
    }

    //called from beforeValidate
    public static function setAuditInfo($model)
    {
        if (static::hasNoUsers())
        {
            $db = Yii::$app->db->createCommand("SELECT DATABASE()")->queryScalar();
            $by = Yii::$app->db->createCommand("SELECT AUTO_INCREMENT FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '" . $db . "' AND TABLE_NAME = 'users'")->queryScalar();
            //$at = \app\models\Location::find()->one()->id;
        }
        else
        {
            static::checkLoginOnSave();
            $by = Yii::$app->user->identity->id;
            //$at = Yii::$app->session->get('currentLocation', false);
            //if (!$at) throw new \yii\web\HttpException(402, 'cannot save until location has been set! Please go to the home page to do so');
        }

        $now = static::now();

        if ($model->getIsNewRecord())
        {
            //$model->added_at = $at;
            $model->added_on = $now;
            $model->added_by = $by;
        }

        //$model->modified_at = $at;
        $model->modified_on = $now;
        $model->modified_by = $by;

        return true;
    }
}