<?php
namespace app\components;

use Yii;
use yii\helpers\Html;
use yii\widgets\DetailView;

//http://www.yiiframework.com/forum/index.php/topic/65977-how-to-write-global-functions-in-yii2-and-access-them-in-any-view-not-the-custom-way/
class View extends \yii\web\View
{
    public function __construct()
    {
        \app\components\AppHelper::init();
        parent::__construct();
    }

    public $targetBlank = ['target' => '_blank'];

    function externalLink($url, $label)
    {
        return sprintf('<li><a href="%s" target="_blank">%s</a></li>', $url, $label);
    }

    function url($slug, $r = 0)
    {
        $url = Yii::$app->urlManager->createUrl($slug);
        if ($r) return $url;
        echo $url;
    }

    function isAdmin()
    {
        return Yii::$app->user->identity != null && Yii::$app->user->identity->admin;
    }

    public static function getLocationLink($model, $for)
    {
        $location = \app\models\Location::findOne(['id' => $for == 'added' ? $model->added_at : $model->modified_at]);
        return Html::a($location->name . ' [' . $location->branch . ']', static::url(['/locations/view', 'id' => $location->id], 1));
    }

    public static function getUserLink($model, $for)
    {
        $user = \app\models\User::findOne(['id' => $for == 'added' ? $model->added_by : $model->modified_by]);
        return Html::a($user->username, static::url(['/users/view', 'id' => $user->id], 1));
    }

    public static function auditDetails($model)
    {
        echo '<h2>Audit Information</h2>';
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                //['attribute' => 'added_at', 'value' => static::getLocationLink($model, 'added'), 'format' => 'html'],
                'added_on:date',
                ['attribute' => 'added_by', 'value' => static::getUserLink($model, 'added'), 'format' => 'html'],
                //['attribute' => 'modified_at', 'value' => static::getLocationLink($model, 'modified'), 'format' => 'html'],
                'modified_on:date',
                ['attribute' => 'modified_by', 'value' => static::getUserLink($model, 'modified'), 'format' => 'html'],
            ],
        ]);
    }
}
?>
