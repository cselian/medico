<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Profile */
app\components\AppFormatter::formatAll($model, $this);

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'type',
            'sub_type',
            'owner_id',
            'name',
            'slug',
            'photo',
            'password',
            'links:raw',
            'id_card',
            'id_card_type',
            'DOB',
            'blood_group',
            'is_blood_donor:boolean',
            'blood_last_donated',
            'blood_last_donated_at',
            'email:email',
            'phone',
            'address',
            'pincode',
            'country',
            'ice',
            'icd',
        ],
    ]) ?>

</div>
