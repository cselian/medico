<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Profiles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Profile', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'type',
            'sub_type',
            'owner_id',
            'name',
            //'slug',
            //'photo',
            //'password',
            //'links',
            //'id_card',
            //'id_card_type',
            //'DOB',
            //'blood_group',
            //'is_blood_donor:boolean',
            //'blood_last_donated',
            //'blood_last_donated_at',
            //'email:email',
            //'phone',
            //'address',
            //'pincode',
            //'country',
            //'ice',
            //'icd',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
