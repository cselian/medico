<?php

/* @var $this yii\web\View */

$this->title = siteTitle();
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome to <?php echo yiiParam('siteName'); ?>!</h1>

        <p class="lead">This is a Medical portal that lets you store your Medical Records safely on a document system like Google / One Drive and easily link your doctor to it  using a QR Code that you can print on a Badge.</p>

        <p><a class="btn btn-lg btn-success" href="http://cselian.com/products/medico/" target="_blank">See Manual</a></p>
    </div>

    <div class="body-content">

        <!-- <div class="row">
            <div class="col-lg-12">
                <?php //\app\models\UserLocation::locationChanger(); ?>
            </div>
        </div> -->

        <div class="row">
            <div class="col-lg-4">
                <h2>Patients</h2>

                <p>Lorem ipsum dolor sit amet consectetuer Morbi tempus condimentum nisl nonummy. Eu congue semper lobortis Duis justo quis vel eros felis justo. Lacinia Nam et fermentum fringilla Donec dui wisi Vestibulum adipiscing et. Congue hendrerit pharetra felis tellus condimentum pharetra dis magnis nunc consequat. Laoreet enim Curabitur scelerisque ante Vivamus ac nec amet tincidunt dapibus.</p>
                <p><a class="btn btn-default" href="<?php $this->url('reports/patients'); ?>">Stats</a></p>
            </div>

            <div class="col-lg-4">
                <h2>Doctors</h2>

                <p>Lorem ipsum dolor sit amet consectetuer Nam euismod mauris metus et. Curabitur porta Curabitur vitae vel hac Maecenas tincidunt orci ac justo. Urna scelerisque quis In Pellentesque euismod vel risus Vivamus mauris lobortis. Ullamcorper In ac cursus Vestibulum justo quam non vel ipsum nibh. Morbi platea auctor ligula Curabitur congue lacinia a ac nibh fermentum. Pellentesque ut.</p>

                <p><a class="btn btn-default" href="<?php $this->url('reports/doctors'); ?>">Stats</a></p>
            </div>

            <div class="col-lg-4">
                <h2>Labs and Hospitals</h2>

                <p>Lorem ipsum dolor sit amet consectetuer habitant pellentesque nec Cum turpis. Et mus tincidunt wisi turpis ut elit feugiat cursus turpis tellus. Nec nibh vitae sagittis Nulla a sapien hendrerit hac Pellentesque vitae. Netus feugiat dui Curabitur Integer et malesuada sodales leo Pellentesque dui. Massa Vestibulum Donec risus Ut Curabitur porttitor lacinia pretium.</p>

                <p><a class="btn btn-default" href="<?php $this->url('reports/orgs'); ?>">Stats</a></p>
            </div>
        </div>

    </div>
</div>
