<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\components\AppHelper;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => yiiParam('siteName'),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $loggedIn = !Yii::$app->user->isGuest;
    $atLocation = $loggedIn && Yii::$app->session->get('currentLocation', false);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            //['label' => 'Create First Location', 'url' => ['/locations/create'], 'visible' => AppHelper::hasNoLocations()],
            ['label' => 'Create First User', 'url' => ['/users/create'], 'visible' => AppHelper::hasNoUsers()],
            ['label' => 'Home', 'url' => ['/site/index'], 'items' => [
              ['label' => 'About', 'url' => ['/site/about']],
              ['label' => 'Contact', 'url' => ['/site/contact']]
            ]],
            ['label' => 'Orders', 'url' => ['/orders'], 'visible' => $atLocation],
            ['label' => 'Customers', 'url' => ['/users/?type=customer'], 'visible' => $atLocation],
            ['label' => 'Reports', 'url' => ['/reports'], 'visible' => $loggedIn],
            ['label' => 'Admin', 'url' => ['#'], 'visible' => $this->isAdmin(), 'items' => [
              ['label' => 'Products', 'url' => ['/products']],
              ['label' => 'Users', 'url' => ['/users']],
              ['label' => 'Locations', 'url' => ['/locations']],
              ['label' => 'Taxes', 'url' => ['/taxes']],
              ['label' => 'Code Gen', 'url' => ['/gii'], 'visible' => yiiParam('isDevMachine'), 'linkOptions' => $this->targetBlank],
              $this->externalLink('http://cselian.com/products/billing/', 'Help'),
            ]],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= yiiParam('siteName')?> 
        <?=yiiParam('siteYear') == date('Y') ? date('Y') : (yiiParam('siteYear') . ' - ' . date('Y')) ?></p>

        <p class="pull-right">
          Built by <a href="http://cselian.com" target="_blank">cselian</a>
          / <?= Yii::powered() ?>
        </p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
