<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%profiles}}".
 *
 * @property int $id
 * @property string $type
 * @property string $sub_type
 * @property int $owner_id
 * @property string $name
 * @property string $slug
 * @property string $photo
 * @property string $password
 * @property string $links
 * @property string $id_card
 * @property string $id_card_type
 * @property string $DOB
 * @property string $blood_group
 * @property bool $is_blood_donor
 * @property string $blood_last_donated
 * @property int $blood_last_donated_at
 * @property string $email
 * @property int $phone
 * @property string $address
 * @property string $pincode
 * @property string $country
 * @property string $ice
 * @property string $icd
 *
 * @property Prescriptions[] $prescriptions
 * @property Prescriptions[] $prescriptions0
 * @property Prescriptions[] $prescriptions1
 * @property Profile $bloodLastDonatedAt
 * @property Profile[] $profiles
 * @property Users $owner
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profiles}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'sub_type', 'owner_id', 'name', 'slug', 'id_card', 'id_card_type', 'DOB', 'ice', 'icd'], 'required'],
            [['owner_id', 'blood_last_donated_at', 'phone'], 'integer'],
            [['DOB', 'blood_last_donated'], 'safe'],
            [['is_blood_donor'], 'boolean'],
            [['type', 'sub_type', 'name', 'slug', 'id_card', 'id_card_type', 'email', 'country'], 'string', 'max' => 128],
            [['photo', 'password', 'address'], 'string', 'max' => 256],
            [['links'], 'string', 'max' => 8192],
            [['blood_group', 'pincode'], 'string', 'max' => 16],
            [['ice', 'icd'], 'string', 'max' => 512],
            [['blood_last_donated_at'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['blood_last_donated_at' => 'id']],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'sub_type' => 'Sub Type',
            'owner_id' => 'Owner ID',
            'name' => 'Name',
            'slug' => 'Slug',
            'photo' => 'Photo',
            'password' => 'Password',
            'links' => 'Links (Badge)',
            'id_card' => 'Id Card',
            'id_card_type' => 'Id Card Type',
            'DOB' => 'Date of Birth',
            'blood_group' => 'Blood Group',
            'is_blood_donor' => 'Is Blood Donor',
            'blood_last_donated' => 'Blood Last Donated',
            'blood_last_donated_at' => 'Blood Last Donated At',
            'email' => 'Email',
            'phone' => 'Phone',
            'address' => 'Address',
            'pincode' => 'Pincode',
            'country' => 'Country',
            'ice' => 'In Case of Emergency',
            'icd' => 'In Case of Death',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrescriptionsAt()
    {
        return $this->hasMany(Prescriptions::className(), ['prescribed_at' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrescriptionsFor()
    {
        return $this->hasMany(Prescriptions::className(), ['patient_for' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrescriptionsBy()
    {
        return $this->hasMany(Prescriptions::className(), ['prescribed_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBloodLastDonatedAt()
    {
        return $this->hasOne(Profile::className(), ['id' => 'blood_last_donated_at']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfiles()
    {
        return $this->hasMany(Profile::className(), ['blood_last_donated_at' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }
}
