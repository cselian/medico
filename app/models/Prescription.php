<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%prescriptions}}".
 *
 * @property int $id
 * @property string $created_on
 * @property string $readings
 * @property string $diagnosis
 * @property string $prescription
 * @property int $patient_for
 * @property int $prescribed_by
 * @property int $prescribed_at
 *
 * @property Profiles $prescribedAt
 * @property Profiles $patientFor
 * @property Profiles $prescribedBy
 */
class Prescription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%prescriptions}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_on', 'diagnosis', 'prescription', 'patient_for', 'prescribed_by'], 'required'],
            [['created_on'], 'safe'],
            [['patient_for', 'prescribed_by', 'prescribed_at'], 'integer'],
            [['readings', 'diagnosis', 'prescription'], 'string', 'max' => 1024],
            [['prescribed_at'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['prescribed_at' => 'id']],
            [['patient_for'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['patient_for' => 'id']],
            [['prescribed_by'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['prescribed_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_on' => 'Created On',
            'readings' => 'Readings',
            'diagnosis' => 'Diagnosis',
            'prescription' => 'Prescription',
            'patient_for' => 'Patient For',
            'prescribed_by' => 'Prescribed By',
            'prescribed_at' => 'Prescribed At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrescribedAt()
    {
        return $this->hasOne(Profile::className(), ['id' => 'prescribed_at']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatientFor()
    {
        return $this->hasOne(Profile::className(), ['id' => 'patient_for']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrescribedBy()
    {
        return $this->hasOne(Profile::className(), ['id' => 'prescribed_by']);
    }
}
