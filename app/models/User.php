<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $auth_key
 * @property string $password_reset_token
 * @property boolean $admin
 * @property string $rights
 * @property string $full_name
 * @property string $email
 * @property string $phone
 * @property boolean $can_email
 * @property boolean $can_sms
 * @property string $added_on
 * @property integer $added_by
 * @property string $modified_on
 * @property integer $modified_by
 *
 * @property Orders[] $orders
 * @property Orders[] $orders0
 * @property UserLocations[] $userLocations
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'added_on', 'added_by', 'modified_on', 'modified_by'], 'required'],
            [['admin', 'can_email', 'can_sms'], 'boolean'],
            [['added_by', 'modified_by'], 'integer'],
            [['added_on', 'modified_on'], 'safe'],
            [['username', 'password'], 'string', 'max' => 45],
            [['auth_key', 'phone'], 'string', 'max' => 32],
            [['password_reset_token'], 'string', 'max' => 255],
            [['rights'], 'string', 'max' => 1024],
            [['full_name', 'email'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'auth_key' => 'Auth Key',
            'password_reset_token' => 'Password Reset Token',
            'admin' => 'Admin',
            'rights' => 'Rights',
            'full_name' => 'Full Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'can_email' => 'Can Email',
            'can_sms' => 'Can Sms',
            'added_on' => 'Added On',
            'added_by' => 'Added By',
            'modified_on' => 'Modified On',
            'modified_by' => 'Modified By',
        ];
    }

    public function load($data, $formName = null)
    {
        $load = parent::load($data, $formName);
        if (!$load) return false;

        if ($this->password == '')
            unset($this->password);
        else
            $this->setPassword($this->password);

        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersAsCustomer()
    {
        return $this->hasMany(Order::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersAsSupplier()
    {
        return $this->hasMany(Order::className(), ['supplier_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLocations()
    {
        return $this->hasMany(UserLocation::className(), ['user_id' => 'id']);
    }

    public function beforeValidate()
    {
        return \app\components\AppHelper::setAuditInfo($this);
    }
    
    //http://www.bsourcecode.com/yiiframework2/yii-2-user-login-from-database/
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
          return static::findOne(['access_token' => $token]);
    }
 
    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Finds user by password reset token
     *
     * @param  string      $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        $expire = \Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === sha1($password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = sha1($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Security::generateRandomKey();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Security::generateRandomKey() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}
