/*
users
profiles
prescriptions
*/

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) DEFAULT NULL,
  `auth_key` varchar(32) DEFAULT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `admin` bit(1) NOT NULL,
  `rights` varchar(1024) DEFAULT NULL,
  `roles` varchar(128) DEFAULT NULL,
  `full_name` varchar(128) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `can_email` bit(1) NOT NULL,
  `can_sms` bit(1) NOT NULL,
  `added_at` int(11) NOT NULL,
  `added_on` datetime NOT NULL,
  `added_by` int(11) NOT NULL,
  `modified_at` int(11) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(128) NOT NULL,
  `sub_type` varchar(128) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `slug` varchar(128) NOT NULL,
  `photo` varchar(256) DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL,
  `links` varchar(8192) DEFAULT NULL,
  `id_card` varchar(128) NOT NULL,
  `id_card_type` varchar(128) NOT NULL,
  `DOB` date NOT NULL,
  `blood_group` varchar(16) DEFAULT NULL,
  `is_blood_donor` bit(1) NOT NULL,
  `blood_last_donated` date DEFAULT NULL,
  `blood_last_donated_at` int(11) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `phone` int(32) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `pincode` varchar(16) DEFAULT NULL,
  `country` varchar(128) DEFAULT NULL,
  `ice` varchar(512) NOT NULL,
  `icd` varchar(512) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_profiles_blood_last_donated_at` (`blood_last_donated_at`),
  KEY `FK_profiles_owner` (`owner_id`),
  CONSTRAINT `FK_profiles_blood_last_donated_at` FOREIGN KEY (`blood_last_donated_at`) REFERENCES `profiles` (`id`),
  CONSTRAINT `FK_profiles_owner` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE `prescriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_on` datetime NOT NULL,
  `readings` varchar(1024) DEFAULT NULL,
  `diagnosis` varchar(1024) NOT NULL,
  `prescription` varchar(1024) NOT NULL,
  `patient_for` int(11) NOT NULL,
  `prescribed_by` int(11) NOT NULL,
  `prescribed_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_for` (`patient_for`,`prescribed_by`,`prescribed_at`),
  KEY `prescribed_by` (`prescribed_by`),
  KEY `prescribed_at` (`prescribed_at`),
  CONSTRAINT `FK_prescriptions` FOREIGN KEY (`prescribed_at`) REFERENCES `profiles` (`id`),
  CONSTRAINT `FK_prescriptions_patient_for` FOREIGN KEY (`patient_for`) REFERENCES `profiles` (`id`),
  CONSTRAINT `FK_prescriptions_prescribed_by` FOREIGN KEY (`prescribed_by`) REFERENCES `profiles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
