select table_name, ordinal_position, column_name, column_type, is_nullable from columns where table_schema = 'medico'

/*
table_name	ordinal_position	column_name	column_type	is_nullable
users	1	id	int(11)	NO
users	2	username	varchar(45)	NO
users	3	password	varchar(45)	YES
users	4	auth_key	varchar(32)	YES
users	5	password_reset_token	varchar(255)	YES
users	6	admin	bit(1)	NO
users	7	rights	varchar(1024)	YES
users	8	roles	varchar(128)	YES
users	9	full_name	varchar(128)	YES
users	10	email	varchar(128)	YES
users	11	phone	varchar(32)	YES
users	12	can_email	bit(1)	NO
users	13	can_sms	bit(1)	NO
users	14	added_at	int(11)	NO
users	15	added_on	datetime	NO
users	16	added_by	int(11)	NO
users	17	modified_at	int(11)	NO
users	18	modified_on	datetime	NO
users	19	modified_by	int(11)	NO
*/
