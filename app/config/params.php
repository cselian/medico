<?php

return configFile('params') ? configFile('params', true) : [
    'siteName' => 'Consumax and Cselian\'s Medico',
    'siteYear' => '2018',
    'adminEmail' => 'dev@cselian.com',
];
