<?php
function configFile($name, $r = false) {
  $file = sprintf('%s/%s-%s.php', __DIR__, $_SERVER['HTTP_HOST'], $name);
  $file = str_replace(':81', '', $file);
  if (!$r) return file_exists($file);
  return include($file);
}

function yiiParam($name, $default = null)
{
  if (!isset(Yii::$app->params[$name]) )
    return $default;
  return Yii::$app->params[$name];
}

function siteTitle($page = '') {
  $name = yiiParam('siteName');
  return $page == '' ? $name : "$page - $name";
}
?>